﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Crime_Search
{
    public class Statute
    {
        private string statute;
        private int chapter;
        private string title;
        private Dictionary<string, int> count;
        public int wordCount;
        private bool favorite;

        public Statute(string file)
        {
            statute = file;
            wordCount = 0;
            favorite = false;
            //makeCount();
            //printDictionary();
        }

        public void printDictionary()
        {
            foreach (KeyValuePair<string, int> kvp in count)
            {
                Console.WriteLine("Key = {0}, Value = {1}",
                    kvp.Key, kvp.Value);
            }
        }

        public void setTitle(string title)
        {
            this.title = title;
        }

        public string getTitle()
        {
            return title;
        }

        public void setChapter(int chapter)
        {
            this.chapter = chapter;
        }

        public int getChapter()
        {
            return chapter;
        }

        public string getStatute()
        {
            return statute;
        }

        public void setFavorite(bool fave)
        {
            this.favorite = fave;  
        }

        public bool isFavorite()
        {
            return favorite;
        }

        public Dictionary<string, int> getCount()
        {
            return count;
        }

        public int getKey(string key)
        {
            if (this.count.ContainsKey(key))
            {
                return count[key];
            }
            return 0;
        }

        public void setWordCount(string word)
        {
            this.wordCount = 0;
            if (count != null && count.ContainsKey(word))
            {
                this.wordCount = count[word.ToLower()];
            }
        }

        public int getWordCount()
        {
            return wordCount;
        }

        public void subString(string search)
        {
                //RichTextBox richTextBox1 = new RichTextBox();
                string s = System.IO.File.ReadAllText(this.statute);
                //var plainText = ConvertToPlainText(s);
                string output;
                output = Regex.Replace(s, "<[^>]*>", string.Empty);

                //get rid of multiple blank lines
                output = Regex.Replace(output, @"^\s*$\n", string.Empty, RegexOptions.Multiline);


                //richTextBox1.Rtf = s;
                //String temp = richTextBox1.Text;
                //richTextBox1.Text = "";
                string temp = output.ToLower();
                if (search != "")
                {
                    this.wordCount = CountWords(temp, search);
                }

                //Console.WriteLine(output);
        }

        private int CountWords(string text, string word)
        {
            int count = (text.Length - text.Replace(word, "").Length) / word.Length;
            Console.WriteLine(count);
            return count;
        }

        public void makeCount()
        {
            RichTextBox box = new RichTextBox();
            count = new Dictionary<string, int>();
            string s = System.IO.File.ReadAllText(this.statute);
            box.Rtf = s;
            string name = box.Text.ToLower();
            var badWords = new string[] { "and", "or", "the", "with", "however", "on" };

            string[] words = name.Split(new char[] { ' ', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ',', '°', '“', '”', '?', '.', ':', ';', '\"', '[', '{', ']', '}', '!', '@', '#', '$', '%', '\n', '\t', '^', '&', '*', '(', ')', '-', '=', '+', '}', '\'', '|', '\\' });

            
            foreach (string wordCount in words)
            {
                if (this.count.ContainsKey(wordCount.ToLower()) == false)
                {
                    this.count.Add(wordCount.ToLower(), 1);
                }
                else
                {
                    try
                    {
                        this.count[wordCount.ToLower()] = count[wordCount.ToLower()] + 1;
                    }
                    catch (KeyNotFoundException e)
                    {
                        System.Console.WriteLine("X" + wordCount + "X");
                        this.count.Add(wordCount, 1);
                    }
                }
               
            }
        }
    }
}



