﻿using HtmlAgilityPack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Sql;
using System.Data.SqlServerCe;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Crime_Search
{
    public partial class CrimeSearch : Form
    {
        private Boolean isNight = false;
        private bool autoCheck;
        private List<Statute> list;

        public CrimeSearch()
        {
            InitializeComponent();
            //updateStatues();
            list = makeStatutes();
            //this.TopMost = true;
            //this.FormBorderStyle = FormBorderStyle.None;
            //this.WindowState = FormWindowState.Maximized;
        }

        public List<Statute> makeStatutes()
        {
            List<Statute> list = new List<Statute>();
            string[] fileEntries = Directory.GetFiles(Application.StartupPath + "\\Statutes\\");
            foreach (string fileName in fileEntries)
            {
                if (!fileName.EndsWith(".html"))
                {
                    continue;
                }
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                var document = new HtmlAgilityPack.HtmlDocument(); 
                doc.Load(new FileStream(fileName, FileMode.Open));

                //string title = doc.DocumentNode.SelectSingleNode("@//div[@class'ChapterName']").InnerText;
                //string chapter = doc.DocumentNode.SelectSingleNode("@//div[@class='ChapterNumber']").InnerText;

                //string title = document.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("ChapterName")).SingleOrDefault().ToString();
                //string chapter = document.DocumentNode.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Contains("ChapterNumber")).SingleOrDefault().ToString();

                string title = doc.DocumentNode.SelectSingleNode(@"//div[@class=""ChapterName""]").InnerText;
                string chapter = doc.DocumentNode.SelectSingleNode(@"//div[@class=""ChapterNumber""]").InnerText;
                //chapter.Replace("Chapter ", "");
                //chapter = chapter.Substring(7, chapter.Length-1);
                chapter = chapter.Replace("CHAPTER ", "");
                int temp; 
                Int32.TryParse(chapter, out temp);
                //Console.WriteLine(temp);

                doc.LoadHtml(fileName);
                WebBrowser webBrowser1 = new WebBrowser();
                Statute statute = new Statute(fileName);
                statute.setChapter(temp);
                statute.setTitle(title);
                //string strHTML = File.ReadAllText(Application.StartupPath + "\\Statutes\\ch" + temp + ".html");
                //webBrowser1.DocumentText = strHTML;
                list.Add(statute);
                //string s = System.IO.File.ReadAllText(fileName);
                //richTextBox1.Rtf = s;
                //string name = richTextBox1.Lines[1];
                //statute.setTitle(name);
                //richTextBox1.Clear();
                //list.Add(statute);
            }
            return list;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO Make the program close when pressed
            Application.Exit();
        }

        private void CrimeSearch_Shown(object sender, EventArgs e)
        {
            //updateList();
            foreach (Statute stat in list)
            {
                //Console.WriteLine(stat.getChapter());
                ListViewItem item = new ListViewItem((stat.getChapter().ToString()));
                item.SubItems.Add(stat.getTitle());
                listView1.Items.Add(item);
                listView1.Font = new Font("Times New Roman", 10.0f);
                listView1.Alignment = ListViewAlignment.Left;
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //richTextBox1.Clear();
            webBrowser1.DocumentText = null;
            string title = null;
            Statute stat = null;
            if (listView1.SelectedItems.Count > 0)
            {
                title = listView1.SelectedItems[0].SubItems[1].Text;
                stat = list.Where(i => i.getTitle() == title).FirstOrDefault();
                string s = System.IO.File.ReadAllText(stat.getStatute());
                //richTextBox1.Rtf = s;
                webBrowser1.DocumentText = System.IO.File.ReadAllText(stat.getStatute());
                if (this.isNight == true)
                {
                    //richTextBox1.SelectionStart = 0;
                    //richTextBox1.SelectionLength = richTextBox1.TextLength;
                    //richTextBox1.SelectionColor = Color.LimeGreen;
                }
                else
                {
                    //richTextBox1.SelectionStart = 0;
                    //richTextBox1.SelectionLength = richTextBox1.TextLength;
                    //richTextBox1.SelectionColor = Color.Blue;
                }

                //HighlightText();
            }
        }

        private void night_Click(object sender, EventArgs e)
        {
            if (isNight)
            {
                this.BackColor = System.Drawing.SystemColors.Control;
                listView1.BackColor = System.Drawing.SystemColors.Control;
                listView1.ForeColor = System.Drawing.Color.Black;
                SearchButton.ForeColor = Color.Black;
                menuStrip3.BackColor = System.Drawing.SystemColors.Control;
                menuStrip3.ForeColor = System.Drawing.Color.Black;
                //richTextBox1.BackColor = Color.White;
                //richTextBox1.ForeColor = Color.Blue;

                ListView.CheckedListViewItemCollection faves = listView1.CheckedItems;
                foreach (ListViewItem item in faves)
                {
                    item.ForeColor = Color.Black;
                    item.BackColor = Color.LightBlue;
                }

                isNight = false;
            }
            else
            {
                this.BackColor = System.Drawing.Color.Black;
                listView1.BackColor = System.Drawing.ColorTranslator.FromHtml("#413E3E");
                listView1.ForeColor = System.Drawing.Color.LimeGreen;
                menuStrip3.BackColor = System.Drawing.ColorTranslator.FromHtml("#413E3E");
                menuStrip3.ForeColor = System.Drawing.Color.LimeGreen;
                //richTextBox1.BackColor = System.Drawing.ColorTranslator.FromHtml("#413E3E");
                //richTextBox1.ForeColor = Color.LimeGreen;

                ListView.CheckedListViewItemCollection faves = listView1.CheckedItems;
                foreach (ListViewItem item in faves)
                {
                    item.ForeColor = Color.Black;
                    item.BackColor = Color.LimeGreen;
                }

                isNight = true;


            }

        }


        //SearchBar
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            //foreach (Statute stat in list)
            //{
            //    //stat.setWordCount(SearchBar.Text);
            //    stat.subString(SearchBar.Text);
            //}
            //list = sort();
            //updateList();

        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            list = sort();
            updateList();
        }

        private List<Statute> sort()
        {
            List<Statute> SortedList;
            if (SearchBar.Text != "")
            {
                foreach (Statute stat in list)
                {
                    stat.subString(SearchBar.Text.ToLower());
                }
                SortedList = list.OrderBy(i => !i.isFavorite()).ThenByDescending(x => x.getWordCount()).ThenBy(z => z.getChapter()).ToList();
            }
            else
            {
                SortedList = list.OrderBy(i => !i.isFavorite()).ThenBy(x => x.getChapter()).ToList();
            }
            return SortedList;
        }

        private void updateList()
        {
            listView1.Items.Clear();
            foreach (Statute stat in list)
            {
                ListViewItem item = new ListViewItem((stat.getChapter().ToString()));
                item.SubItems.Add(stat.getTitle());
                if (stat.isFavorite())
                {
                    item.Checked = true;
                    if (isNight != true)
                    {
                        item.BackColor = Color.LightBlue;
                    }
                    else
                    {
                        item.BackColor = Color.LimeGreen;
                        item.ForeColor = Color.Black;
                    }
                }
                listView1.Items.Add(item);
                listView1.Font = new Font("Times New Roman", 10.0f);
                listView1.Alignment = ListViewAlignment.Left;

                Console.WriteLine(stat.getTitle());
            }
        }

        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //if (autoCheck)
            //e.NewValue = e.CurrentValue;
        }

        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            string title = null;
            Statute stat = null;
            if (listView1.SelectedItems.Count > 0)
            {
                title = listView1.SelectedItems[0].SubItems[1].Text;
                stat = list.Where(i => i.getTitle() == title).FirstOrDefault();
                stat.setFavorite(listView1.SelectedItems[0].Checked);
                list = sort();
                updateList();
            }
        }

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            autoCheck = true;
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            autoCheck = false;
        }

        public void HighlightText()
        {

        }

        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HtmlWeb web = new HtmlWeb();
            int count = 1;
            while (true)
            {
                if (count > 1013)
                {
                    break;
                }
                HtmlAgilityPack.HtmlDocument doc;
                try
                {
                    string c = count.ToString().PadLeft(4, '0');

                    int toNearest = 100;
                    int value = count;
                    int rest = value % toNearest;
                    string high = (value - rest).ToString().PadLeft(4, '0');
                    string low = (count + (toNearest - rest - 1)).ToString().PadLeft(4, '0');
                    doc = web.Load("http://www.leg.state.fl.us/Statutes/index.cfm?App_mode=Display_Statute&URL=" + high + "-" + low + "/" + c + "/" + c + ".html");

                }
                catch (Exception e2)
                {
                    count++;
                    continue;
                }
                string path = Application.StartupPath + "\\Statutes\\ch" + count + ".html";
                string dashboard;
                try
                {
                    dashboard = doc.DocumentNode.SelectSingleNode("//div[@id='statutes']").InnerHtml;

                }
                catch (Exception e1)
                {
                    count++;
                    continue;
                }
                doc.LoadHtml(dashboard);
                doc.Save(path);
                dashboard.ToString().Trim();
                string strHTML = File.ReadAllText(Application.StartupPath + "\\Statutes\\ch" + count + ".html");
                WebBrowser webBrowser1 = new WebBrowser();
                webBrowser1.DocumentText = strHTML;
                count++;
            }

            //    public void HighlightText()
            //    {
            //        try
            //        {
            //            if (richTextBox1.Text != string.Empty)
            //            {// if the ritchtextbox is not empty; highlight the search criteria
            //                int index = 0;
            //                String temp = richTextBox1.Text;
            //                richTextBox1.Text = "";
            //                richTextBox1.Text = temp.ToLower();
            //                while (index < richTextBox1.Text.LastIndexOf(SearchBar.Text))
            //                {
            //                    richTextBox1.Find(SearchBar.Text, index, richTextBox1.TextLength, RichTextBoxFinds.None);
            //                    richTextBox1.SelectionBackColor = Color.Yellow;
            //                    index = richTextBox1.Text.IndexOf(SearchBar.Text, index) + 1;

            //                    richTextBox1.Select();
            //                }
            //            }
            //        }

            //        catch (Exception ex) { MessageBox.Show(ex.Message, "Error"); }
            //    }
        }
   }
}

